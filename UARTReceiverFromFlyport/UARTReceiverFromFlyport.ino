/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 25;

int blinkStat=true;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT); 
Serial.begin(19200);  
// Serial3.begin(19200);
Serial.write("serial initialized"); 
}

  char cmd2[200];
  char cmd[200];
// the loop routine runs over and over again forever:
void loop() {
  int i=0; 
  while(Serial.available()>0) 
  cmd[i++]=Serial.read(); 
  cmd[i]=0;
  Serial.write(cmd);
  
  int a, b, c, d; //each from 0 to 1000
  //int rd=sscanf(cmd, "%d.%d %d.%d %d.%d", &a, &d, &b, &d, &c, &d);
  int rd=sscanf(cmd, "%d ", &a);
  //if(rd>0)Serial.print(rd);
  if (rd==1) 
  {
    sprintf(cmd2, " interpreted %d %d %d int end\n", a, b, c);
    Serial.print(cmd2);
  }
  if(
  (blinkStat && a+b+c>100) || 
  (!blinkStat && a+b+c<100)
  ) {blinkStat=!blinkStat;
  //Serial.write("blinked\n");
  //blinkStat=!blinkStat;
}
  digitalWrite(led, blinkStat);   // turn the LED on (HIGH is the voltage level)
  //delay(1000);               // wait for a second
}

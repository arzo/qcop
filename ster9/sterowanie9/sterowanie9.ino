#include <SoftwareSerial.h>

#include <Servo.h>
#include <stdio.h>

#include <FastSerial.h>
#include <AP_Common.h>
#include <Arduino_Mega_ISR_Registry.h>
#include <AP_GPS.h>             // ArduPilot GPS library
#include <I2C.h>                // Arduino I2C lib
#include <SPI.h>                // Arduino SPI lib
#include <SPI3.h>               // SPI3 library
#include <AP_ADC.h>             // ArduPilot Mega Analog to Digital Converter Library
#include <AP_AnalogSource.h>
#include <AP_Baro.h>            // ArduPilot Mega Barometer Library
#include <Filter.h>
#include <AP_Compass.h>         // ArduPilot Mega Magnetometer Library
#include <AP_Math.h>            // ArduPilot Mega Vector/Matrix math Library
#include <AP_InertialSensor.h>  // ArduPilot Mega Inertial Sensor (accel & gyro) Library
#include <AP_PeriodicProcess.h> // Parent header of Timer
#include <AP_TimerProcess.h>    // TimerProcess is the scheduler for MPU6000 reads.
#include <AP_AHRS.h>
#include <AP_Airspeed.h>
#include <AC_PID.h>             // PID library
#include <APM_PI.h>             // PID library
#include <AP_InertialNav.h>
#include <AP_Buffer.h>          // ArduPilot general purpose FIFO buffer
#include <AC_PID.h> // ArduPilot Mega RC Library



// create a FILE structure to reference our UART output function
//int minK0= 102+1+3;//103 , 110
//int minK1= 102+1+3; //103+1, 110+1
//int minK0= 107+5+5;//103 , 110
//int minK1= 107+5+5; //103+1, 110+1

int serwN3=2;  //1
int serwN4=3;  //0

struct QMotors {
  char pwms[4];
  char pins[4];
  int DEBUG; //0 nie wypisuj, 1 wypisuj dane
  Servo servos[4];
  boolean flagU[4];
  int armedFrom[4];
  int maxValue[4];
  int armed;
  int minK[4];
  int INIT;
  int initEngine[4];
  QMotors() {}
  //as in APM, R=1, L=2, F=3, B=4 
  //suggested (PB6, PB5, PH5, PH4) => in arduino pins notation: (12, 11, 8, 7)
  QMotors(int pinR, int pinL, int pinF, int pinB) {
    armed=0;
    pins[0]=pinR;
    pins[1]=pinL;
    pins[2]=pinF;
    pins[3]=pinB;
    for(int i=0; i<4; ++i) servos[i].write(0);
    armedFrom[0]=20;
    armedFrom[1]=41;
    armedFrom[2]=22+  5;
    armedFrom[3]=19+1+5;
    for(int i=0; i<4; ++i) flagU[i]=false;
    for(int i=0; i<4; ++i) pwms[i]=0;
   
    maxValue[0]=armedFrom[0]+20;
    maxValue[1]=armedFrom[1]+20;
    maxValue[2]=armedFrom[2]+20; 
    maxValue[3]=armedFrom[3]+20;
    DEBUG=0;
    minK[0]=armedFrom[0];
    minK[1]=armedFrom[1];
    minK[2]=armedFrom[2];
    minK[3]=armedFrom[3];
    INIT=0;
         
  }
  //ustaw moc silnika na @pow zakres setPow zakres (0..100)
  int setPow(int engine_id, int pow) {
    if (pow<0 || pow>100) Serial.printf("SET POW ERROR\n"); else  {
    pwms[engine_id]=armedFrom[engine_id]+(int)pow*(maxValue[engine_id]-armedFrom[engine_id])/100;
    servos[engine_id].write(armed?pwms[engine_id]:0);
    }
  }
  int setPowVal(int engine_id, int val) {
      pwms[engine_id]=val;
      servos[engine_id].write(armed?pwms[engine_id]:0);
  }
  //zwieksz moc silnika o @incBy setPow zakres (0..100)
  int incPow(int engine_id, int incBy) {
    //if (pwms[engine_id]+incBy>100 || pwms[engine_id]+incBy<0) {Serial.printf("INC POW ERROR\n");} else 
    //{
     if(armed)
     {
      pwms[engine_id]+=incBy;
      servos[engine_id].write(pwms[engine_id]);
     }
     //else
     //  Serial.printf("NOT ARMED\n!");
  }
  
  void init() {
    for(int i=0; i<4; ++i) {
      servos[i].attach(pins[i]);
      Serial.printf("Attached [%d] to pin %d.\n", i, pins[i]+1);
    }
    INIT = 1;
  }
  void writeAll(int val) {
    for(int i=0; i<4; ++i) servos[i].write(pwms[i]=val);
  }
  void arm(bool arm) {
    
    armed=arm;
    if(INIT==1)
    {
      for(int i=0; i<4; ++i) initEngine[i]=10;//armedFrom[i]-27;
      for(int i=0; i<4; i++) {
        setPowVal(i,initEngine[i]);
        Serial.printf("wrote max %d %d\n",i, pwms[i]);
      }
      delay(3000);
      
      for(int i=0; i<4; i++) {
         setPowVal(i,0);
         Serial.printf("wrote min %d %d\n",i, pwms[i]);
      }
        delay(3000); 
    }
   
    if(arm)
    {
      for(int i=0; i<4; i++) 
      {
       setPow(i,5);
        Serial.printf("wrote run %d %d\n",i, pwms[i]);
      }
    }    
    Serial.printf("Engines initialized!\n");
  }

  void incSchodk(int val, int nr, int przec, int limitU)
  {
    int i=0;
    int count=2*limitU;
    for(i;i<val;i++)
    {
      //Serial.printf("dzialaf\n", nr);
        if(flagU[nr])
        {
           if(DEBUG) Serial.printf("1Nr %d,flagU true\n", nr);
           if(pwms[przec] > minK[przec]) //
           {             
             incPow(przec,-1);
             if (DEBUG) Serial.printf("11Nr %d,flagU true\n", nr);
           }
           flagU[przec]=false;   //nawet proba dekremetancji zmniejszy z true na false flage, wiec da wieksza inercje przy wzroscie
           flagU[nr]=false;
                      
        }
        else //false
        {
          if (DEBUG) Serial.printf("2Nr %d ,flagU false\n", nr);
          if(pwms[nr] < minK[nr]+ limitU)
          {
            incPow(nr,+1);
            if(DEBUG) Serial.printf("22Nr %d ,flagU false\n", nr);
          }
          flagU[nr]=true;   
        }     
    }
  }
  void incSchodk2(int val, int nr, int przec, int limitU, int skok1, int skok2)
  {
    int i=0;
    int count=2*limitU;
    for(i;i<val;i++)
    {
         //Serial.printf("dzialaf\n", nr);
        if(flagU[nr])
        {
           if(DEBUG) Serial.printf("1Nr %d,flagU true\n", nr);
           if(pwms[przec] > minK[przec]) //
           {             
             incPow(przec,-skok2);
             if (DEBUG) Serial.printf("11Nr %d,flagU true\n", nr);
           }
           flagU[przec]=false;   //nawet proba dekremetancji zmniejszy z true na false flage, wiec da wieksza inercje przy wzroscie
           flagU[nr]=false;
                      
        }
        else //false
        {
          if (DEBUG) Serial.printf("2Nr %d ,flagU false\n", nr);
          if(pwms[nr] < minK[nr]+ limitU)
          {
            incPow(nr,+skok1);
            if(DEBUG) Serial.printf("22Nr %d ,flagU false\n", nr);
          }
          flagU[nr]=true;   
        }     
      
    }
  }
  
};

static FILE uartout = {0} ;
FastSerialPort(Serial, 0);
QMotors qmotors;

Arduino_Mega_ISR_Registry isr_registry;
AP_TimerProcess scheduler;
AP_InertialSensor_MPU6000 ins;

void setupMotors() {
  qmotors=QMotors(12, 11, 8, 7);
  Serial.write("Initialized motors.\n");
  qmotors.init(); //those are run parrarely. 
  qmotors.arm(true);
}

static int uart_putchar (char c, FILE *stream)
{
    Serial.write(c) ;
    return 0 ;
}

void setup() {
    Serial.begin(115200);
    //while(!Serial);
    fdev_setup_stream (&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);
   // The uart is the standard output device STDOUT.
    stdout = &uartout ;
    Serial.println("Doing INS startup...");
    SPI.begin();
    SPI.setClockDivider(SPI_CLOCK_DIV16); // 1MHZ SPI rate
    isr_registry.init();
    scheduler.init(&isr_registry);
    // we need to stop the barometer from holding the SPI bus
    pinMode(40, OUTPUT);
    digitalWrite(40, HIGH);
    ins.init(AP_InertialSensor::COLD_START, 
			 AP_InertialSensor::RATE_100HZ,
			 delay, NULL, &scheduler);
    //for(int i=0; i<4; ++i) qmotors.servos[i].write(0);
    setupMotors();
     
    //wartosci poczatkowe
    Serial.write("initialized.\n");
}

char koniec='k';
// default PID values
#define TEST_P 1.0
#define TEST_I 0.01
#define TEST_D 0.2
#define TEST_IMAX 10

void loop() {
    char t[200];
    int index;
    int val,nr;
  
    Vector3f accel;
    Vector3f gyro;
    float temperature;
    float zm;
    int czyK=0;
    int akcel;
    //AC_PID pid(TEST_P, TEST_I, TEST_D, TEST_IMAX * 100);
    uint16_t radio_in;
    uint16_t radio_trim;
    int error;
    int control;
    float dt = 1000/50;
    float deltaPX=0;
    float deltaX=0;
    boolean DISP=false;
    int licznik=0;
    // display PID gains
   // Serial.printf("P %f  I %f  D %f  imax %f\n", pid.kP(), pid.kI(), pid.kD(), pid.imax());
    int ile2=0;
    float blad=0.4;
    float blad2=1.0;
    int val1,val2;
    float fval1,fval2;
    int run='n';
    int ile=0;
    char buf[200];
    int sk1=1;
    int sk2=1;
    int limit1=1;
    int limit2=1;
    int pause=1;
    Serial.printf("Zmieniono na qyro %f, acc%f\n", blad, blad2);        
    while(1) {

        ins.update();
        gyro = ins.get_accel();
        accel = ins.get_gyro();
        temperature = ins.temperature();
       
        
        zm =  gyro.y;
       // Serial.printf("AX: %5f  AY: %5f  AZ: %5f  GX: %5f  GY: %5f  GZ: %5f T=%5f\n",
       //               accel.x, accel.y, accel.z, gyro.x, gyro.y, gyro.z, temperature);
        //Serial.printf("GX: %4f  GY: %f  GZ: %f T=%f\n", gyro.x, gyro.y, gyro.z, temperature);
        //Serial.printf("gyro:%f acc:%f,", gyro.x, accel.y);
        licznik++;
        //if((licznik%5==0)&&pause==0)   {  DISP=true;  }
       // else         {  DISP=false;   }
        if(DISP&&pause==0)Serial.printf("gyro:%f acc:%f,", gyro.y, accel.x);
       //------------------------------algorytm sterowania----------------------------------------//
        //radio_in = APM_RC.InputCh(0);
        //error = zm;
        //control = pid.get_pid(error, dt);
        //if(zm < -0.25)
        deltaX=accel.x;

 
                  
  /*        if(zm>blad)
          {
              qmotors.incSchodk(1, serwN3,serwN4,limit1); //
              //qmotors.incSchodk2(1, serwN3,serwN4,limit1,2,2);
          }
          if(zm < -blad)       
          {
              qmotors.incSchodk(1, serwN4,serwN3,limit2);
              //qmotors.incSchodk2(1, serwN4,serwN3,limit2,2,2);
          }
           //qmotors.incSchodk2(1, serwN4,serwN3,minK0, minK1,1,2,1); */
       if( pause==0)
        {
       if( 0 <=deltaX < blad2)
        {          
          if(zm>blad)
          {
              qmotors.incSchodk(1, serwN3,serwN4,limit1); //
              //qmotors.incSchodk2(1, serwN3,serwN4,limit1,2,2);
          }
          if(zm < -blad)       
          {
              qmotors.incSchodk(1, serwN4,serwN3,limit2);
              //qmotors.incSchodk2(1, serwN4,serwN3,limit2,2,2);
          } 
            //qmotors.incSchodk2(1, serwN3,serwN4,minK0, minK1,1,2,1); 
       } 
       
         if( 0< deltaX&& deltaX > -blad2)
          {          
            if(zm>blad)
            {
                qmotors.incSchodk(1, serwN3,serwN4,limit1); //
                //qmotors.incSchodk2(1, serwN3,serwN4,limit1,2,2);
            }
            if(zm < -blad)       
            {
                qmotors.incSchodk(1, serwN4,serwN3,limit2);
                //qmotors.incSchodk2(1, serwN4,serwN3,limit2,2,2);
            } 
              //qmotors.incSchodk2(1, serwN3,serwN4,minK0, minK1,1,2,1); 
         } 
        }
         //delay(100);
        //------------------------------algorytm sterowania----------------------------------------//
        //Serial.printf(" s1: %d,s2: %d, s3: %d, s4: %d\n",qmotors.pwms[0],qmotors.pwms[1], qmotors.pwms[serwN3],  qmotors.pwms[serwN4]); 
        if(DISP) Serial.printf(" s1: %d,s2: %d, s3: %d, s4: %d\n",qmotors.pwms[0],qmotors.pwms[1], qmotors.pwms[2],  qmotors.pwms[3]);    
        //Serial.printf("radio: %d\t err: %d\t pid:%d\n", radio_in, error, control); 
        //---------------------------------wylacznik----------------------------------------------//
        #define READINPUT(t)        index=0; while(Serial.available() > 0) t[index++] = Serial.read(); 
        READINPUT(t);       
        if(index>0) {
          t[index] = '\0';  
          if(t[0]=='b')
            {
              qmotors.writeAll(0);
              koniec=t[0];  
              Serial.printf("Koniec!!!\n");
            }
            ile=sscanf(t, "%s %d %d",buf, &val1, &val2);
            if(strcmp(buf,"c")==0)
            {
               qmotors.minK[2]=val1;
               qmotors.minK[3]=val2;
            }
            else if(strcmp(buf,"arm")==0) {            
               qmotors.arm(1);
               DISP=true; 
               pause=0;
               qmotors.DEBUG=1; //ponowne wlaczenie wyswietlania po p
            }
            else if(strcmp(buf,"arm1")==0) {                          
               qmotors.INIT=1;
               DISP=true;
                pause=0;
               qmotors.arm(1);               
               qmotors.INIT=0;  
               qmotors.DEBUG=1;

            }
            else if(strcmp(buf,"disarm")==0)
               qmotors.arm(0);
            else if(strcmp(buf,"p")==0){
               qmotors.arm(0); 
               pause=1;
               qmotors.writeAll(0);
               DISP=false;
               qmotors.DEBUG=0;
            }
            else if(strcmp(buf,"lim")==0){
                limit1=val1;
                limit2=val2;
            }
            else if(strcmp(buf,"b")==0) {
              qmotors.writeAll(0);
              Serial.printf("Koniec, zamknal!!!\n");
              Serial.end();
              while(1); //TODO wznawianie pracy? 
            }
            else if(strcmp(buf,"w")==0)
               Serial.printf(" s1: %d,s2: %d, s3: %d, s4: %d\n",qmotors.pwms[0],qmotors.pwms[1], qmotors.pwms[2],  qmotors.pwms[3]);
            ile2=sscanf(t, "%s %f %f",buf, &val1, &val2);
            if(strcmp(buf,"bl")==0) {
                //if(ile2==2)          
                 // blad2=fval2;
                //if(ile2==3)
                // {
                    blad=fval1;
                    blad2=fval2;
                // }
                 Serial.printf("Zmieniono na qyro %f, acc %f\n", fval1, fval2);             
           }
               
        }
        //Serial.printf("wartosc zm wynosi %c\n", koniec); 
      }
}

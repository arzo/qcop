struct PID {
  float pgain;
  float igain;
  float dgain;
  float pvalue;
  float ivalue;
  float dvalue;
  float max_ivalue;
  float last;
  PID() {
    pgain = 0;
    igain = 0;
    dgain = 0;
  }
  PID(float _pgain, float _igain, float _dgain) {
    pgain = _pgain;
    igain = _igain;
    dgain = _dgain; 
  }
  void setMaxIValue(float value) {
     max_ivalue = value; 
  }
  void resetError() {
    ivalue = 0;
  }
  float updatePID(float target, float current, float dt) {
    float error = target - current; 
    
    pvalue = error;
    ivalue += error * deltaTime;
    
    if (ivalue > max_ivalue) 
      ivalue = max_ivalue;
    else if (ivalue < - max_ivalue) 
      ivalue = - max_ivalue;

    dvalue = (cur - last) / dt;

    last = current;
    
    return pvalie * pgain + ivalue * igain - dvalue * dgain;
  }
}

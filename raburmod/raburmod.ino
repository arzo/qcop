
#include <SoftwareSerial.h>

#include <Servo.h>
#include <stdio.h>

#include <FastSerial.h>
#include <AP_Common.h>
#include <Arduino_Mega_ISR_Registry.h>
#include <AP_GPS.h>             // ArduPilot GPS library
#include <I2C.h>                // Arduino I2C lib
#include <SPI.h>                // Arduino SPI lib
#include <SPI3.h>               // SPI3 library
#include <AP_ADC.h>             // ArduPilot Mega Analog to Digital Converter Library
#include <AP_AnalogSource.h>
#include <AP_Baro.h>            // ArduPilot Mega Barometer Library
#include <Filter.h>
#include <AP_Compass.h>         // ArduPilot Mega Magnetometer Library
#include <AP_Math.h>            // ArduPilot Mega Vector/Matrix math Library
#include <AP_InertialSensor.h>  // ArduPilot Mega Inertial Sensor (accel & gyro) Library
#include <AP_PeriodicProcess.h> // Parent header of Timer
#include <AP_TimerProcess.h>    // TimerProcess is the scheduler for MPU6000 reads.
#include <AP_AHRS.h>
#include <AP_Airspeed.h>
#include <AC_PID.h>             // PID library
#include <APM_PI.h>             // PID library
#include <AP_InertialNav.h>
#include <AP_Buffer.h>          // ArduPilot general purpose FIFO buffer
#include <AC_PID.h> // ArduPilot Mega RC Library


#define INIT_GYRO_SAMPLES 300
#define GYRO_PRECISION 10
#define ACC_PRECISION 100
#define ENGINE_MAX 100

#define CONTROL_SIGNAL_AMPLITUDE 10

#define GYRO_kP 0.2
#define GYRO_kD 0.05
#define GYRO_kI 0.0

struct PID {
  float pgain;
  float igain;
  float dgain;
  float pvalue;
  float ivalue;
  float dvalue;
  float max_ivalue;
  float last;
  PID() {
    pgain = 0;
    igain = 0;
    dgain = 0;
  }
  PID(float _pgain, float _igain, float _dgain) {
    pgain = _pgain;
    igain = _igain;
    dgain = _dgain; 
  }
  void setMaxIValue(float value) {
     max_ivalue = value; 
  }
  void resetError() {
    ivalue = 0;
  }
  float updatePID(float target, float current, float dt) {
    float error = target - current; 
    
    pvalue = error;
    ivalue += error * dt;
    
    if (ivalue > max_ivalue) 
      ivalue = max_ivalue;
    else if (ivalue < - max_ivalue) 
      ivalue = - max_ivalue;

    dvalue = (current - last) / dt;

    last = current;
    
    return pvalue * pgain + ivalue * igain - dvalue * dgain;
  }
};



struct QMotors {
  float pwms[4];
  char pins[4];
  int DEBUG; //0 nie wypisuj, 1 wypisuj dane
  Servo servos[4];
  boolean flagU[4];
  int engineMax[4];
  int engineMin[4];
  int armed;
  int INIT;
  int initEngine[4];
  QMotors() {}
  //as in APM, R=1, L=2, F=3, B=4 
  //suggested (PB6, PB5, PH5, PH4) => in arduino pins notation: (12, 11, 8, 7)
  QMotors(int pinR, int pinL, int pinF, int pinB) {
    armed=0;
    pins[0]=pinR;
    pins[1]=pinL;
    pins[2]=pinF;
    pins[3]=pinB;
    for(int i=0; i<4; ++i) servos[i].write(0);
    engineMin[0]=56;
    engineMin[1]=42;
    engineMin[2]=1;
    engineMin[3]=1;
    for(int i=0; i<4; ++i) flagU[i]=false;
    for(int i=0; i<4; ++i) pwms[i]=0;
   
    engineMax[0]=engineMin[0]+100;
    engineMax[1]=engineMin[1]+100;
    engineMax[2]=1; 
    engineMax[3]=1;
    DEBUG=0;
    
    INIT=0;
         
  }
  float pwmVal(int engine_id) {
     return pwms[engine_id];
  }
  //ustaw moc silnika na @pow zakres setPow zakres (0..100)
  int setPow(int engine_id, int pow) {
    pow = (pow > 100 ? 100 : pow);
    if (pow < 0) {
      pwms[engine_id] = 0;
    } else {
      pwms[engine_id] = engineMin[engine_id]+pow*(engineMax[engine_id]-engineMin[engine_id])/100.0;
    }
    servos[engine_id].write((char)pwms[engine_id]);
  }
  int setPowVal(int engine_id, int val) {
      pwms[engine_id]=val;
      servos[engine_id].write((char)pwms[engine_id]);
  }
  //zwieksz moc silnika o @incBy setPow zakres (0..100)
  int incPow(int engine_id, float incBy) {
      pwms[engine_id]+=incBy;
      servos[engine_id].write((char)pwms[engine_id]);
  }
  
  void init() {
    for(int i=0; i<4; ++i) {
      servos[i].attach(pins[i]);
      Serial.printf("Attached [%d] to pin %d.\n", i, pins[i]+1);
    }
    INIT = 1;
  }
  void writeAll(int val) {
    // CHANGED HERE
    for(int i=0; i<4; ++i) 
      setPow(i, val);
  }
  void arm(bool arm) {
    
    armed=arm;
    if(INIT==1)
    {
      for(int i=0; i<4; ++i) initEngine[i]=10;
      for(int i=0; i<4; i++) {
        setPowVal(i,initEngine[i]);
        printf("wrote max %d %d\n",i, pwms[i]);
      }
      delay(3000);
      
      for(int i=0; i<4; i++) {
         setPowVal(i,0);
         printf("wrote min %d %d\n",i, pwms[i]);
      }
        delay(3000); 
    }   
    printf("Engines initialized!\n");
  }
};
static FILE uartout = {0} ;
FastSerialPort(Serial, 0);
QMotors qmotors;

Arduino_Mega_ISR_Registry isr_registry;
AP_TimerProcess scheduler;
AP_InertialSensor_MPU6000 ins;

void setupMotors() {
  qmotors=QMotors(12, 11, 8, 7);
  Serial.write("Initialized motors.\n");
  qmotors.init(); //those are run parrarely. 
}

static int uart_putchar (char c, FILE *stream)
{
    Serial.write(c) ;
    return 0 ;
}

void calibrateGyro(Vector3f *originGyro) {
  Vector3f gyro;
  int counter = 0;
  for(counter = 0; counter < INIT_GYRO_SAMPLES; counter++) {
       ins.update();
       gyro = ins.get_accel();
       //accel = ins.get_gyro();
       //temperature = ins.temperature();
       originGyro->x += gyro.x;
       originGyro->y += gyro.y;
       originGyro->z += gyro.z;
    }
    originGyro->x /= INIT_GYRO_SAMPLES;
    originGyro->y /= INIT_GYRO_SAMPLES;
    originGyro->z /= INIT_GYRO_SAMPLES;
}

float engineForceChangeFromControlSignal(float signal) {
  // linear
  signal = signal > CONTROL_SIGNAL_AMPLITUDE ? CONTROL_SIGNAL_AMPLITUDE : (signal < - CONTROL_SIGNAL_AMPLITUDE ? - CONTROL_SIGNAL_AMPLITUDE : signal);
  return signal;// / CONTROL_SIGNAL_AMPLITUDE * ENGINE_MAX;
  // semi-logarythmic
  if (signal > 0) {
    return log(signal / CONTROL_SIGNAL_AMPLITUDE + 0.1) * 0.96 * ENGINE_MAX;
  } else {
    return - (log( - signal / CONTROL_SIGNAL_AMPLITUDE + 0.1) * 0.96 * ENGINE_MAX);
  }
}

// Declarations
    int appState = 0;
    // 0 - iddle
    // 1 - calibrate gyro
    // 2 - arm
    // 10 - start
    int counter = 0;
    int val = 0, nr = 0;
    float temperature, dt;
    unsigned long time;
    
    float engineInc[4] = {0,0,0,0};
    Vector3f appendForce;

    
    Vector3f acc;
    Vector3f gyro;
    Vector3f originGyro;
    // Inits

    PID pid01;
    
    int command = 0, i = 0;
    char t[200];


void setup() {
    Serial.begin(115200);
    //while(!Serial);
    fdev_setup_stream (&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);
   // The uart is the standard output device STDOUT.
    stdout = &uartout ;
    //Serial.println("Doing INS startup...");
    SPI.begin();
    SPI.setClockDivider(SPI_CLOCK_DIV16); // 1MHZ SPI rate
    isr_registry.init();
    scheduler.init(&isr_registry);
    // we need to stop the barometer from holding the SPI bus
    pinMode(40, OUTPUT);
    digitalWrite(40, HIGH);
    ins.init(AP_InertialSensor::COLD_START, 
			 AP_InertialSensor::RATE_100HZ,
			 delay, NULL, &scheduler);
    appendForce.x = 0;
    appendForce.y = 0;
    appendForce.z = 0;
    
    originGyro.x = 0;
    originGyro.y = 0;
    originGyro.z = 0;
    
    pid01 = PID(GYRO_kP, GYRO_kI, GYRO_kD);
    
    setupMotors();
    
    // Auto calibrating Gyro  
    calibrateGyro(&originGyro);
    // Arming
    //qmotors.arm(true);
    //wartosci poczatkowe
    printf("Starting up finished.\n");
    printf("Command:\n   0 - Stop all\n   1 - Calibrate gyro\n   2 - Arm\n   3 - Inc thrust\n   4 - Dec thrust\n   8 - Print current motor values\n   9 - Start\n");
}

void loop() {
    int command = -1;
    i = 0;
    while(Serial.available()) t[i++]=Serial.read();
    t[i]='\0';
    if (i > 0) {
      sscanf(t, "%d", &command);
      printf("read: %d\n", command);
    }
    // Just a quick menu
    switch (command) {
      case 1:
        printf("Calibrating gyro...\n");
        calibrateGyro(&originGyro);
        printf("Calibrated.\n");
        break;
      case 2:
        printf("Arming...\n");
        qmotors.arm(true);
        break;
      case 3:
        appendForce.z += 5;
        break;
      case 4:
        appendForce.z -= 5;
        break;
      case 8:
        printf("Motor values: %f, %f, %f, %f\n", qmotors.pwmVal(0),  qmotors.pwmVal(1), qmotors.pwmVal(2), qmotors.pwmVal(3));
        break;
      case 9:
        printf("Starting...\n");
        time = micros();
        pid01.resetError();
        appendForce.x = 0;
        appendForce.y = 0;
        appendForce.z = 0;
        appState = 9;
        qmotors.writeAll(1);
        break;
      case 0:
        qmotors.writeAll(-1);
        appState = 0;
        break;
    }
    
 
    if(appState == 9) {
       // updating gyro, acc and temperature data
       dt = (micros() - time) / 1000000.0;
       time = micros();
       
       ins.update();
       gyro = ins.get_accel();
       acc = ins.get_gyro();
       temperature = ins.temperature();
       
       // Passing Gyro via low pass filter
       gyro.x = round((gyro.x - originGyro.x)*GYRO_PRECISION)/GYRO_PRECISION;
       gyro.y = round((gyro.y - originGyro.y)*GYRO_PRECISION)/GYRO_PRECISION;
       gyro.z = round((gyro.z - originGyro.z)*GYRO_PRECISION)/GYRO_PRECISION;
       
       // Passing Acc via low pass filter
       //acc.x = round(acc.x*ACC_PRECISION)/ACC_PRECISION;
       //acc.y = round(acc.y*ACC_PRECISION)/ACC_PRECISION;
       //acc.z = round(acc.z*ACC_PRECISION)/ACC_PRECISION;
       
      float value01 = pid01.updatePID(0, gyro.y, dt);

       // UWAGA: Zakładam, że silniki są tak ponumerowane jak w docu strona 8, QUAD +
        printf("na 1: %d\n", -value01*100);
        engineInc[0] = engineForceChangeFromControlSignal(value01);
        engineInc[1] = engineForceChangeFromControlSignal(-value01);
 //       engineInc[2] = engineForceChangeFromControlSignal(controlSignal( - gyro.x, - gyroD.x, acc.x, appendForce.z, dt));
 //       engineInc[3] = engineForceChangeFromControlSignal(controlSignal(gyro.x, gyroD.x, acc.x, appendForce.z, dt));
       
        qmotors.incPow(0, engineInc[0]);
        qmotors.incPow(1, engineInc[1]);
        //qmotors.incPow(engineToSerwo[2], engineInc[2]);
        //qmotors.incPow(engineToSerwo[3], engineInc[3]);
   }
}

#include <SoftwareSerial.h>

#include <Servo.h>
#include <stdio.h>

// we need fundamental FILE definitions and printf declarations
#include <stdio.h>

// create a FILE structure to reference our UART output function
#define POSITION_VALUES_MULTIPLER 1000
static FILE uartout = {0} ;

// create a output function
// This works because Serial.write, although of
// type virtual, already exists.
static int uart_putchar (char c, FILE *stream)
{
    Serial.write(c) ;
    return 0 ;
}

void setupPrintf(void)
{
   // Start the UART
   //if(!Serial) 
   Serial.begin(115200);
   while(!Serial);
   Serial.write("initialized UART\n");

   // fill in the UART file descriptor with pointer to writer.
   fdev_setup_stream (&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);

   // The uart is the standard output device STDOUT.
   stdout = &uartout ;
}



struct QMotors {
  float pwms[4];
  char pins[4];
  int DEBUG; //0 nie wypisuj, 1 wypisuj dane
  Servo servos[4];
  boolean flagU[4];
  int engineMax[4];
  int engineMin[4];
  int armed;
  int INIT;
  int initEngine[4];
  QMotors() {}
  //as in APM, R=1, L=2, F=3, B=4 
  //suggested (PB6, PB5, PH5, PH4) => in arduino pins notation: (12, 11, 8, 7)
  QMotors(int pinR, int pinL, int pinF, int pinB) {
    armed=0;
    pins[0]=pinR;
    pins[1]=pinL;
    pins[2]=pinF;
    pins[3]=pinB;
    for(int i=0; i<4; ++i) servos[i].write(0);
    engineMin[0]=56;
    engineMin[1]=42;
    engineMin[2]=1;
    engineMin[3]=1;
    for(int i=0; i<4; ++i) flagU[i]=false;
    for(int i=0; i<4; ++i) pwms[i]=0;
   
    engineMax[0]=engineMin[0]+100;
    engineMax[1]=engineMin[1]+100;
    engineMax[2]=1; 
    engineMax[3]=1;
    DEBUG=0;
    
    INIT=0;
         
  }
  float pwmVal(int engine_id) {
     return pwms[engine_id];
  }
  //ustaw moc silnika na @pow zakres setPow zakres (0..100)
  int setPow(int engine_id, int pow) {
    pow = (pow > 100 ? 100 : pow);
    if (pow < 0) {
      pwms[engine_id] = 0;
    } else {
      pwms[engine_id] = engineMin[engine_id]+pow*(engineMax[engine_id]-engineMin[engine_id])/100.0;
    }
    servos[engine_id].write((char)pwms[engine_id]);
  }
  int setPowVal(int engine_id, int val) {
      pwms[engine_id]=val;
      servos[engine_id].write((char)pwms[engine_id]);
  }
  //zwieksz moc silnika o @incBy setPow zakres (0..100)
  int incPow(int engine_id, float incBy) {
      pwms[engine_id]+=incBy;
      servos[engine_id].write((char)pwms[engine_id]);
  }
  
  void init() {
    for(int i=0; i<4; ++i) {
      servos[i].attach(pins[i]);
      //Serial.printf("Attached [%d] to pin %d.\n", i, pins[i]+1);
    }
    INIT = 1;
  }
  void writeAll(int val) {
    // CHANGED HERE
    for(int i=0; i<4; ++i) 
      setPow(i, val);
  }
  void arm(bool arm) {
    
    armed=arm;
    if(INIT==1)
    {
      for(int i=0; i<4; ++i) initEngine[i]=10;
      for(int i=0; i<4; i++) {
        setPowVal(i,initEngine[i]);
        printf("wrote max %d %d\n",i, pwms[i]);
      }
      delay(3000);
      
      for(int i=0; i<4; i++) {
         setPowVal(i,0);
         printf("wrote min %d %d\n",i, pwms[i]);
      }
        delay(3000); 
    }   
    printf("Engines initialized!\n");
  }
};

//preconditions: Serial is running! 
struct CommandReceiver {
  void (*callbacks[10])(const char *);
  int n;
  CommandReceiver():n(0) {
  }
  CommandReceiver &addListener(void (*listener)(const char *)) {
    callbacks[n]=listener;
    n+=1;
    return *this;
  }
  
};

QMotors qmotors;
void setupMotors() {
  qmotors=QMotors(12, 11, 8, 7);
  Serial.write("Initialized motors.\n");
  qmotors.init(); //those are run parrarely. 
  qmotors.arm(true);
}

void (*func[5]) (void)={setupPrintf, setupMotors, NULL};


void setup() {
  int i=0;
  while(func[i]) func[i++]();
}

void loop() {
  if(Serial.available()) {
    char t[200];
    sprintf(t, "");
    int i=0;
    while(Serial.available()) t[i++]=Serial.read();
    t[i]='\0';
    //printf("wczytano %s\n", t);
    int x, y, z;
    int ile;
    char command[20];
    ile=sscanf(t, "%s", &command);
    if (command[0] == 'g') {
      ile=sscanf(t, "g %d %d df", &x, &y, &z);
      if(ile == 3) {
        int newVal[4];
        newVal[0] = z  * 100 / POSITION_VALUES_MULTIPLER;
        newVal[1] = z * 100 / POSITION_VALUES_MULTIPLER;
        newVal[2] = z * 100 / POSITION_VALUES_MULTIPLER;
        newVal[3] = z * 100 / POSITION_VALUES_MULTIPLER;
        if (x > 0) {
           newVal[0] = newVal[0] + x * 50 / POSITION_VALUES_MULTIPLER; 
        } else {
           newVal[1] = newVal[1] + x * - 50 / POSITION_VALUES_MULTIPLER; 
        }
        if (y > 0) {
           newVal[2] = newVal[2] + y * 50 / POSITION_VALUES_MULTIPLER; 
        } else {
           newVal[3] = newVal[3] + y * - 50 / POSITION_VALUES_MULTIPLER; 
        }
        qmotors.setPow(0, newVal[0]);
        qmotors.setPow(1, newVal[1]);
        qmotors.setPow(2, newVal[2]);
        qmotors.setPow(3, newVal[3]);
      }
    } else if (command[0] == 'a') { // arm
      qmotors.arm(true);
    } else if (command[0] == 'd') { // arm
      qmotors.arm(false);
    }
  } 
}

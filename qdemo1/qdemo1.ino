#include <SoftwareSerial.h>

#include <Servo.h>
#include <stdio.h>

// we need fundamental FILE definitions and printf declarations
#include <stdio.h>

// create a FILE structure to reference our UART output function

static FILE uartout = {0} ;

// create a output function
// This works because Serial.write, although of
// type virtual, already exists.
static int uart_putchar (char c, FILE *stream)
{
    Serial.write(c) ;
    return 0 ;
}

void setupPrintf(void)
{
   // Start the UART
   //if(!Serial) 
   Serial.begin(115200);
   while(!Serial);
   Serial.write("initialized UART\n");

   // fill in the UART file descriptor with pointer to writer.
   fdev_setup_stream (&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);

   // The uart is the standard output device STDOUT.
   stdout = &uartout ;
}


struct QMotors {
  char pwms[4];
  char pins[4];
  Servo servos[4];
  QMotors() {}
  //as in APM, R=1, L=2, F=3, B=4 
  //suggested (PB6, PB5, PH5, PH4) => in arduino pins notation: (12, 11, 8, 7)
  QMotors(int pinR, int pinL, int pinF, int pinB) {
    pins[0]=pinR;
    pins[1]=pinL;
    pins[2]=pinF;
    pins[3]=pinB;
  }
  void init() {
    for(int i=0; i<4; ++i) {
      servos[i].attach(pins[i]);
      printf("Attached %d to pin %d.\n", i, pins[i]);
    }
  }
  void writeAll(int val) {
    for(int i=0; i<4; ++i) servos[i].write(val);
  }
  void arm(bool arm) {
    
    writeAll(10);
    printf("wrote max; ");
    delay(3000);
    
    writeAll(0);
    printf("wrote min\n");
    delay(3000);
    
    printf("Engines initialized!\n");
  }
};

//preconditions: Serial is running! 
struct CommandReceiver {
  void (*callbacks[10])(const char *);
  int n;
  CommandReceiver():n(0) {
  }
  CommandReceiver &addListener(void (*listener)(const char *)) {
    callbacks[n]=listener;
    n+=1;
    return *this;
  }
  
};

QMotors qmotors;
void setupMotors() {
  qmotors=QMotors(12, 11, 8, 7);
  Serial.write("Initialized motors.\n");
  qmotors.init(); //those are run parrarely. 
  qmotors.arm(true);
}

void (*func[5]) (void)={setupPrintf, setupMotors, NULL};


void setup() {
  int i=0;
  while(func[i]) func[i++]();
}

void loop() {
  if(Serial.available()) {
    char t[200];
    sprintf(t, "");
    int i=0;
    while(Serial.available()) t[i++]=Serial.read();
    t[i]='\0';
    //printf("wczytano %s\n", t);
    int val,nr;
    int ile;
    ile=sscanf(t, "%d %d", &val, &nr);
    printf("v=%d nr=%d\n");
    //if(ile==2) 
    if(nr >= 0 && nr <= 3)
      qmotors.servos[nr].write(val);
     // else 
    //qmotors.writeAll(val);
    //printf("wczytano %d\n", ile);
    printf("wpisano %d do silnika %d\n", val, nr);
  } else 
  delay(500);
}
